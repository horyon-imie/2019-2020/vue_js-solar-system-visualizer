import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    allData: null,
    largestObjects: null,
    smallestObjects: null,
    bestMoons: null,
  },
  getters: {
    allData: (state) => state.allData,
    largestObjects: (state) => state.largestObjects,
    smallestObjects: (state) => state.smallestObjects,
    bestMoons: (state) => state.bestMoons,
  },
  mutations: {
    setAllData(state, data) {
      state.allData = data;
    },
    setLargestObjects(state, data) {
      state.largestObjects = data;
    },
    setSmallestObjects(state, data) {
      state.smallestObjects = data;
    },
    setBestMoons(state, data) {
      state.bestMoons = data;
    },
  },
  actions: {},
  modules: {},
});
